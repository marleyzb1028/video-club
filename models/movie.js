const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _title:String,
    _genre: Genre,
    _director: Director,
    _cast: Actor
});

class Movie {
    constructor(title, genre, director, cast){
        this._title = title;
        this._genre = genre;
        this._director = director;
        this._cast = cast;
    }
    get title(){
        return this._title;
    }
    set title(m){
        this._title = m;
    }
    get genre(){
        return this._genre;
    }
    set genre(m){
        this._genre = m;
    }
    get director(){
        return this._director;
    }
    set director(m){
        this._director = m;
    }
    get cast(){
        return this._cast;
    }
    set cast(m){
        this._cast = m;
    }
}

schema.loadClass(Movie);
module.exports = mongoose.model('Movie', schema);