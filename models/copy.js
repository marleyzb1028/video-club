const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _number: String,
    _format: {
        VHS,
        DVD,
        BLU_RAY
    },
    _movie: Movie,
    _status: {
        AVAILABLE,
        RENTED
    }
});

class Copy {
    constructor(number, format, movie, status){
        this._number = number;
        this._format = format;
        this._movie = movie;
        this._status = status;
    }
    get number(){
        return this._number;
    }
    set number(m){
        this._number = m;
    }
    get format(){
        return this._format;
    }
    set format(m){
        this._format = m;
    }
    get movie(){
        return this._movie;
    }
    set movie(m){
        this._movie = m;
    }
    get status(){
        return this._status;
    }
    set status(m){
        this._status = m;
    }
}

schema.loadClass(Copy);
module.exports = mongoose.model('Copy', schema);