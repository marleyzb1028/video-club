const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _date: Date,
    _member: Member,
    _copy: Copy
});

class Booking {
    constructor(date, member, copy){
        this._date = date;
        this._member = member;
        this._copy = copy;
    }
    get date(){
        return this._date;
    }
    set date(m){
        this._date = m;
    }
    get member(){
        return this._member;
    }
    set member(m){
        this._member = m;
    }
    get copy(){
        return this._copy;
    }
    set copy(m){
        this._copy = m;
    }
}

schema.loadClass(Booking);
module.exports = mongoose.model('Booking', schema);