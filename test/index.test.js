const supertest = require('supertest');

const app = require('../app');

//Sentencia
describe("Probar el sistema de autenticación", ()=>{
    it("Debería obtener un login con usuario y contraseña correctos", (done)=>{
        supertest(app).post("/login")
        .send({'email': 'marleyzb@uach.mx', 'password': '12345'})
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });

    it("No debería obtener un login con usuario y contraseña correctos", (done)=>{
        supertest(app).post("/login")
        .send({'email': 'marleyzb1@uach.mx', 'password': '12345'})
        .expect(403)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});