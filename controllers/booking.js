const express = require('express');
const { Booking } = require('../models/booking');
const { Copy } = require('../models/copy');
const { Member } = require('../models/member');

function list(req, res, next) {
    Booking.find().then(obj => res.status(200).json({
        message: res.__('ok.listBooking'),
        createdBooking: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listBooking'),
        error: err
    }));

}
function index(req, res, next) {
    const id = req.params.id;
    Booking.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listBooking'),
        createdBooking: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listBooking'),
        error: err
    }));

}
async function create(req, res, next) {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copyId = req.body.copyId;
    let member = await Member.findOne({_id:memberId });
    let copy = await Copy.findOne({_id:copyId });
    let booking = new Booking({
        date: date,
        member: member,
        copy: copy
    });

    Booking.save().then(obj => res.status(200).json({
        message: res.__('ok.createBooking'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createBooking'),
        obj: err
    }));
}
function replace(req, res, next) {
    const id = req.params.id;
    let date = req.body.date ? req.body.date : " ";
    let booking = new Object({
        _date :date,
    });
    Booking.findOneAndUpdate({_id:id},booking, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateBooking'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateBooking'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let date = req.body.date;
    let booking = new Object();
    if(date){
        booking._date = date;
    }
    Booking.findOneAndUpdate({_id:id},booking).then(obj => res.status(200).json({
        message: res.__('ok.updateBooking'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateBooking'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Booking.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteBooking'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteBooking'),
        obj: err
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
}; 