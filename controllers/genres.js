const express = require('express');
const { Genre } = require('../db');

function list(req, res, next) {
    Genre.find().then(obj => res.status(200).json({
        message: res.__('ok.listGenre'),
        createdGenre: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listGenre'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    Genre.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listGenre'),
        createdGenre: obj
    })).catch(err => res.status(500).json({
        message: res.__(`bad.listGenre`),
        error: err
    }));
}
function create(req, res, next) {
    const description = req.body.description;
    const status = req.body.status;
    let Genre = new Object({
        description: description,
        status: status
    });
    Genre.save().then(obj => res.status(200).json({
        message: res.__('ok.createGenre'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createGenre'),
        obj: err
    }));
}
function replace(req, res, next) {
    const id = req.params.id;
    let description = req.body.description ? req.body.description : " ";
    let status = req.body.status ? req.body.status : false;
    let Genre = new Object({
        _description :description,
        _status:status
    });
    Genre.findOneAndUpdate({_id:id}, Genre, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateGenre'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateGenre'),
        obj: err
    }));
}
function update(req, res, next) {
    res.send('respond with a update');
}
function destroy(req, res, next) {
    res.send('respond with a destroy');
}

module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
}; 