const express = require('express');
const { AwaitList } = require('../models/awaitList');

function list(req, res, next) {
    AwaitList.find().then(obj => res.status(200).json({
        message: res.__('ok.listaAwaitList'),
        createdAwaitList: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaAwaitList'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    AwaitList.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listaAwaitList'),
        createdAwaitList: obj
    })).catch(err => res.status(500).json({
        message: res.__(`bad.listaAwaitList`),
        error: err
    }));
}
function create(req, res, next) {
    const memberId = req.body.memberId;
    const movieId = req.body.movieId;

    let awaitList = new AwaitList({
        memberId: memberId,
        movieId:movieId
    });
    awaitList.save().then(obj => res.status(200).json({
        message: res.__('ok.createAwaitList'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createAwaitList'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    AwaitList.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteAwaitList'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteAwaitList'),
        obj: err
    }));
}

module.exports = {
    list,
    index,
    create,
    destroy
}