const express = require('express');
const { Actor } = require('../models/actor');

function list(req, res, next) {
    Actor.find().then(obj => res.status(200).json({
        message: res.__('ok.listActor'),
        createdActor: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listActor'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Actor.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listActor'),
        createdActor: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listActor'),
        error: err
    }));
}


function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;

    let actor = new Actor({
        name: name,
        lastName:lastName
    });
    actor.save().then(obj => res.status(200).json({
        message: res.__('ok.createActor'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createActor'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let lastName = req.body.lastName ? req.body.lastName : " ";

    
    
    let actor = new Object({
        _name :name,
        _lastName:lastName
    });

    Actor.findOneAndUpdate({_id:id},actor, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateActor'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateActor'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

 
    
    let actor = new Object();

    if(name){
        genre._name = name;
    }
    if(lastName){
        genre._lastName = lastName;
    }

    Actor.findOneAndUpdate({_id:id},actor).then(obj => res.status(200).json({
        message: res.__('ok.updateActor'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateActor'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Actor.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteActor'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteActor'),
        obj: err
    }));
}


module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
}