const express = require('express');
const { Member } = require('../models/member');

function list(req, res, next) {
    Member.find().then(objs =>res.status(200).json({
        message: res.__('ok.listMember'),
        obj: objs
    }))
    .catch(ex => res.status(500).json({
        message: res.__('bad.listMember'),
        obj: ex
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    Member.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listMember'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listMember'),
        error: err
    }));
}
function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    let address = new Object();
    address.street = req.body.street;
    address.number = req.body.number;
    address.zip = req.body.zip;
    address.state = req.body.state;

    let member = new Member({
        name: name,
        lastName: lastName,
        phone: phone,
        address: address
    });

    member.save().then(obj => res.status(200).json({
        message: res.__('ok.createMember'),
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: res.__('bad.createMember'),
        obj: ex
    }));
}
function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let lastName = req.body.lastName ? req.body.lastName : " ";
    let phone = req.body.phone ? req.body.phone : " ";
    let address = new Object();
    address.street = req.body.street ? req.body.street : " ";
    address.number = req.body.number ? req.body.number : " ";
    address.zip = req.body.zip ? req.body.zip : 0;
    address.state = req.body.state ? req.body.state : " ";
    let Member = new Object({
        _name :name,
        _lastName:lastName,
        _phone:phone,
        _address:address
     
    });
    Member.findOneAndUpdate({_id:id}, Member, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMember'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let phone = req.body.phone;
    let address = new Object();
    address.street = req.body.street;
    address.number = req.body.number;
    address.zip = req.body.zip;
    address.state = req.body.state;
    let Member = new Object();
    if(name){
        Member._name = name;
    }
    if(lastName){
        Member._lastName = lastName;
    }
    if(phone){
        Member._phone = phone;
    }
    if(address){
        Member._address = address;
    }
    Member.findOneAndUpdate({_id:id}, Member).then(obj => res.status(200).json({
        message: res.__('ok.updateMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMember'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Member.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteMember'),
        obj: err
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
};