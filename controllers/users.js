const express = require('express');
const bcrypt = require('bcrypt');
const User = require('./')

function list(req, res, next) {
    User.find().then(obj => res.status(200).json({
        message: res.__('ok.listUser'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listUser'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    User.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getUser'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getUser'),
        error: err
    }));
}
async function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    const salt = await bcrypt.genSalt(10);

    const passwordHash = await bcrypt.hash(password, salt);

    let user = new User({
        name: name,
        lastName: lastName,
        email: email,
        password: passwordHash,
        saltKey: salt
    });

    User.save().then(obj => res.status(200).json({
        message: res.__('ok.createUsuario'),
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: res.__('bad.createUsuario'),
        obj: ex
    }));
}
async function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let lastName = req.body.lastName ? req.body.lastName : " ";
    let email = req.body.email ? req.body.email : " ";
    let password = req.body.password ? req.body.password : " ";
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password,salt);
    let User = new Object({
        _name :name,
        _lastName :lastName,
        _email :email,
        _password :passwordHash,
        _salt: salt
    });
    User.findOneAndUpdate({_id:id},User, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateUser'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateUser'),
        obj: err
    }));
}
async function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName ;
    let email = req.body.email;
    let password = req.body.password;
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password,salt);
    let User = new Object({
        _name :name,
        _lastName :lastName,
        _email :email,
        _password :passwordHash,
        _salt: salt
    });
    User.findOneAndUpdate({_id:id},User, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateUser'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateUser'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    User.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteUser'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteUser'),
        obj: err
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
};