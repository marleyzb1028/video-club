const express = require('express');
const { Director } = require('../models/director');

function list(req, res, next) {
    Director.find().then(obj => res.status(200).json({
        message: res.__('ok.listDirector'),
        createdDirector: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listDirector'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    Director.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listDirector'),
        createdDirector: obj
    })).catch(err => res.status(500).json({
        message: res.__(`bad.listDirector`),
        error: err
    }));
}
function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    let director = new Director({
        name: name,
        lastName:lastName
    });
    director.save().then(obj => res.status(200).json({
        message: res.__('ok.createDirector'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createDirector'),
        obj: err
    }));
}
function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let lastName = req.body.lastName ? req.body.lastName : " ";
    let director = new Object({
        _name :name,
        _lastName:lastName
    });
    director.findOneAndUpdate({_id:id},director, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateDirector'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateDirector'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let director = new Object();
    if(name){
        director._name = name;
    }
    if(lastName){
        director._lastName = lastName;
    }
    director.findOneAndUpdate({_id:id},director).then(obj => res.status(200).json({
        message: res.__('ok.updateDirector'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateDirector'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Director.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteDirector'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteDirector'),
        obj: err
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
};