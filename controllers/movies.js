const express = require('express');
const { Movie } = require('../models/movie');
const { Genre } = require('../models/genre');
const { Actor } = require('../models/actor');
const { Director } = require('../models/director');

function list(req, res, next) {
    Movie.find().populate("_genre").then(obj => res.status(200).json({
        message: res.__('ok.listMovie'),
        createdMovie: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listMovie'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    Movie.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.listMovie'),
        createdMovie: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listMovie'),
        error: err
    }));
}
async function create(req, res, next) {
    const title = req.body.title;
    const genreId = req.body.genreId;
    const directorId = req.body.directorId;
    const actorId = req.body.actorId;
    let genre = await Genre.findOne({_id:genreId});
    let director = await Director.findOne({_id:directorId});
    let actor = await Actor.findOne({_id:actorId});
    let Movie = new Movie({
        title: title,
        genre:genre,
        director,
        actor
    });
    Movie.save().then(obj => res.status(200).json({
        message: res.__('ok.createMovie'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createMovie'),
        obj: err
    }));
}
function replace(req, res, next) {
    const id = req.params.id;
    let description = req.body.description ? req.body.description : " ";
    let status = req.body.status ? req.body.status : false;
    let Movie = new Object({
        _description :description,
        _status:status
    });
    Movie.findOneAndUpdate({_id:id}, Movie, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateMovie'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMovie'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let description = req.body.description;
    let status = req.body.status;
    let Movie = new Object();
    if(description){
        Movie._description = description;
    }
    if(status){
        Movie._status = status;
    }
    Movie.findOneAndUpdate({_id:id},Movie).then(obj => res.status(200).json({
        message: res.__('ok.updateMovie'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMovie'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    let description = req.body.description;
    let status = req.body.status;
    let Movie = new Object();
    if(description){
        Movie._description = description;
    }
    if(status){
        Movie._status = status;
    }
    Movie.findOneAndUpdate({_id:id},Movie).then(obj => res.status(200).json({
        message: res.__('ok.updateMovie'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMovie'),
        obj: err
    }));
}
 
module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
};