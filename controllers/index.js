const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');

function home(req, res, next) {
    res.render('index', { title: 'Express' });
}
function login(req, res, next){
    const email = req.body.email;
    const password = req.body.password;
 
    User.findOne({"_email": email}).select('_password _salt')
            .then((user) => {
                if(user){
                    bcrypt.hash(password, user.salt, (err, hash)=>{
                        if(err){
                            res.status(403).json({
                                message: "No se pudo realizar el login",
                                obj: err
                            });
                        }
                        if(hash === user.password){
                            const jwtKey = config.get("secret.key");
                            res.status(200).json({
                                message: res.__('ok.login'),
                                obj: jwt.sign({exp: Math.floor(Date.now()/1000)+60}, jwtKey)
                            });
                        }else{
                            res.status(403).json({
                                message: res.__('bad.login'),
                                obj: null
                            });
                        }
                    });
                }else{
                    res.status(403).json({
                        message: res.__('bad.login'),
                        obj: null
                    });
                }
            }).catch(err => {
                res.status(403).json({
                    message: "No se pudo realizar el login",
                    obj: null
                });
            });
}

module.exports = {
    home, login
}; 