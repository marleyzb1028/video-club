const express = require('express');
const { Copy } = require('../models/copy');

function list(req, res, next) {
    Copy.find().then(obj => res.status(200).json({
        message: res.__('ok.listCopy'),
        createdCopy: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listCopy'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params.id;
    Copy.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.findCopy'),
        createdCopy: obj
    })).catch(err => res.status(500).json({
        message: res.__(`bad.findCopy`),
        error: err
    }));
}

module.exports = {
    list,
    index
};