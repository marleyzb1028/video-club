const Sequelize = require('sequelize');

const genreModel = require('./models/genre');
const actorModel = require('./models/actor');
const bookingModel = require('./models/booking');
const copyModel = require('./models/copy');
const directorModel = require('./models/director');
const memberModel = require('./models/member');
const movieModel = require('./models/movie');
const userModel = require('./models/user');

// 1) Nombre de la base de datos
// 2) Usuario de la base de datos
// 3) Contraseña de la base de datos
// 4) Objeto de configuración (ORM)

// No usar root, por efecto del ejemplo se usa
const sequelize = new Sequelize('video_club', 'root', 'secret', {
    host: '127.0.0.1',
    dialect: 'mysql'
});
                        // conexxión, type
const Genre = genreModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const User = userModel(sequelize, Sequelize);

sequelize.sync({
    force: true
}).then(()=>{
    console.log('La base de datos ha sido actualizada');
});

module.exports = { Genre, Actor, Booking, Copy, Director, Member, Movie, User };