FROM node
MAINTAINER Marley Zaragoza
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start